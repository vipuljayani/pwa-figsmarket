export default interface GoogleRecaptchaState {
  google_generated_token?: null|string,
  is_verified?: null|boolean
}
