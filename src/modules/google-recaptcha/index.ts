import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { GoogleRecaptchaModuleStore } from './store/index';
import { afterRegistration } from './hooks/afterRegistration'

const KEY = 'googleRecaptcha';

export const GoogleRecaptchaModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule(KEY, GoogleRecaptchaModuleStore)
  afterRegistration(appConfig, store)
};
