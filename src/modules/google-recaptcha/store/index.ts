import { Module } from 'vuex'
import GoogleRecaptchaState from '../types/GoogleRecaptchaState'
import actions from './actions'
import mutations from './mutations'

export const GoogleRecaptchaModuleStore: Module<GoogleRecaptchaState, any> = {
  namespaced: true,
  state: {
    google_generated_token: '',
    is_verified: false
  },
  mutations,
  actions
}
