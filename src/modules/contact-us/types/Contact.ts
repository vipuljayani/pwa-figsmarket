export default interface Contact {
  about: string[],
  name: string,
  email: string,
  phonenumber: string,
  comment: string
}
