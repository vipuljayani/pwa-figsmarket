import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { ContactUsModuleStore } from './store/index';
export const KEY = 'contact'

export const ContactUsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule(KEY, ContactUsModuleStore)
};
