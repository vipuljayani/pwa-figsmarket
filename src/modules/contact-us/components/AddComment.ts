import Contact from 'src/modules/contact-us/types/Contact'

export const AddComment = {
  name: 'AddComment',
  methods: {
    addComment (contact: Contact) {
      this.$store.dispatch('contact/add', contact)
    }
  }
}
