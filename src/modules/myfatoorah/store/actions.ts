import Vue from 'vue'
import MyFatoorahState from '../types/MyFatoorahState'
import { ActionTree } from 'vuex';
import * as types from './mutation-types';
import rootStore from '@vue-storefront/core/store'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers'
import { Logger } from '@vue-storefront/core/lib/logger'

export const actions: ActionTree<MyFatoorahState, any> = {
  async initiatePayment ({ commit, rootState }, request) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.myfatoorah.initiate_endpoint)
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            commit(types.MYFATOORAH_INITIATE_PAYMENT, data.result.Data.PaymentMethods);
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'address-book')()
          }
        })
    } catch (e) {
      Logger.error(e, 'myfatoorah')()
    }
  },
  async executePayment ({ commit, rootState }, request) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.myfatoorah.execute_endpoint)
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            commit(types.MYFATOORAH_EXECUTE_PAYMENT, data.result.Data);
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'address-book')()
          }
          return data;
        })
    } catch (e) {
      Logger.error(e, 'myfatoorah')()
    }
  },
  async directPayment ({ commit, rootState }, request) {
    try {
      let url = processLocalizedURLAddress(rootStore.state.config.myfatoorah.direct_endpoint)
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          'apiURL': request.apiURL,
          'card': {
            'paymentType': request.paymentType,
            'card': {
              'Number': request.cardNumber,
              'expiryMonth': request.cardExpiryMonth,
              'expiryYear': request.cardExpiryYear,
              'securityCode': request.cardSecurityCode
            },
            'saveToken': true
          }
        })
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            commit(types.MYFATOORAH_DIRECT_PAYMENT, data.result.Data);
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'address-book')()
          }
          return data;
        })
    } catch (e) {
      Logger.error(e, 'myfatoorah')()
    }
  },
  async setCCInfo ({ commit }, ccdata) {
    commit(types.MYFATOORAH_CC_INFO, ccdata)
  }
}
