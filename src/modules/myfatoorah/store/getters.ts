import MyFatoorahState from '../types/MyFatoorahState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<MyFatoorahState, any> = {
  getMethods: (state) => state.methods,
  getInvoice: (state) => state.invoice,
  getResponse: (state) => state.response,
  getCCData: (state) => state.ccdata
}
