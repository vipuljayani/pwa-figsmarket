export default interface CardDetails {
  paymentMethodId: number | string,
  paymentType: string,
  cardNumber: number | string,
  cardExpiryMonth: number | string,
  cardExpiryYear: number | string,
  cardSecurityCode: number,
  cardHolderName: string,
  apiURL: string
}
