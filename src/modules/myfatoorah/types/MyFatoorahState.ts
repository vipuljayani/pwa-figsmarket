import CardDetails from './CardDetails';

export default interface MyFatoorahState {
  methods: any[],
  ccdata: CardDetails,
  invoice: any[],
  response: any[]
}
