import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { MyFatoorahModuleStore } from './store';

export const MyFatoorahModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('myfatoorah', MyFatoorahModuleStore)
};
