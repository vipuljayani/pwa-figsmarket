import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { CustomerModuleStore } from './store/index';

export const CustomerModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('customer', CustomerModuleStore)
};
