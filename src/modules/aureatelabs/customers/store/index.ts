import { Module } from 'vuex'
import CustomerState from '../types/CustomerState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const CustomerModuleStore: Module<CustomerState, any> = {
  namespaced: true,
  state: {
    tokens: [],
    token: []
  },
  mutations,
  actions,
  getters
}
