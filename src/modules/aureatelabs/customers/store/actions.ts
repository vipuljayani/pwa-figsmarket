import { ActionTree } from 'vuex';
import CustomerState from '../types/CustomerState';
import * as types from './mutation-types';
import rootStore from '@vue-storefront/core/store'
import { Logger } from '@vue-storefront/core/lib/logger'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'

const actions: ActionTree<CustomerState, any> = {

  /**
   * Retrieve customer tokens
   */
  async listTokens (context, email) {
    try {
      let url = rootStore.state.config.aureatelabs.customer.listTokens.replace('{{customerEmail}}', email)
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }

      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.CUSTOMER_FETCH_TOKENS, data.result.items)
          }
        })
    } catch (e) {
      Logger.error(e)()
    }
  },

  /**
   * Store customer tokens
   */
  async storeToken (context, tokenData) {
    try {
      let url = rootStore.state.config.aureatelabs.customer.storeToken
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }

      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(tokenData)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            context.commit(types.CUSTOMER_STORE_TOKEN, data.result)
          }
        })
    } catch (e) {
      Logger.error(e)()
    }
  }
};
export default actions;
