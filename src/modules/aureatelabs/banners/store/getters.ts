import BannerState from '../types/BannerState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<BannerState, any> = {
  getBannerList: (state) => state.banners,
  hasItems: (state) => state.banners && state.banners.length > 0
}
