import { ActionTree } from 'vuex';
import BannerState from '../types/BannerState';
import * as types from './mutation-types';
import SearchQuery from '@vue-storefront/core/lib/search/searchQuery';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
import config from 'config';

const alBannerEntityName = config.aureatelabs.es_key;
const actions: ActionTree<BannerState, any> = {
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */
  async list ({ getters, commit }, { filterValues = null, filterField = 'id', size = 10, start = 0, excludeFields = null, includeFields = null, skipCache = false }) {
    if (skipCache || !getters.hasItems) {
      let bannerQuery = new SearchQuery()
      if (filterValues) {
        bannerQuery = bannerQuery.applyFilter({ key: filterField, value: { like: filterValues } })
      }

      const blockResponse = await quickSearchByQuery({
        query: bannerQuery,
        entityType: 'aureate_banner',
        size,
        start,
        excludeFields,
        includeFields
      })

      commit(types.BANNER_FETCH_BANNER, blockResponse.items)
      return blockResponse.items
    }

    return getters.getBannerList
  }
};
export default actions;
